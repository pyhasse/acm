===================
Module: pyhasse.acm
===================

* Free software: MIT license
* Documentation: https://pyhasse.org


Features
--------

Calculate:

- antichain matrix (ACM)
- rowsum of ACM
- colsum of ACM
