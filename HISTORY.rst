=======
History
=======

0.2.7 (2019-03-27)
------------------

- improved desecription for better search results with "pip search pyhasse"

0.2.6 (2019-03-24)
------------------

- URL for repository added

0.2.5 (2019-03-21)
------------------

- classifier added
- wrong pointer corrected


0.2.4 (2018-03-05)
------------------

- set long_description_content_type to text/x-rst


0.1.0 (2018-06-10)
------------------

* First version
