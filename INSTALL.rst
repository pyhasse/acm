pyhasse.acm
============

Getting Started
---------------

- Change directory into your newly created project.

  .. code::
  
     cd /go/to/your/project/folder

- Create a Python virtual environment.

  .. code::

     python3 -m venv pyhasse

- Upgrade packaging tools.

  .. code::

    env/bin/pip install --upgrade pip setuptools
    env/bin/pip install --upgrade pip pip

- Install pyhasse.acm

  .. code::
    
     pip install pyhasse.acm
    
- Read the documentation

  You can find the documentation at:
  
  https://pyhasse.org
