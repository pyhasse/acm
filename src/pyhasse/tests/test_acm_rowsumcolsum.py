# content of test_acm.py
"""Tests for acm.py"""
from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.acm.calc import ACM
import os
import pytest

@pytest.fixture
def data():
    TESTFILENAME = '/data/chain_pollution.csv'
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath+TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    acm = ACM(matrix.data, matrix.rows, matrix.cols)
    return acm, matrix


def test_acm(data):
    """Test all operations in acm"""
    acm = data[0]
    matrix = data[1]
    
    assert matrix.prop == ["Pb", "Cd", "Zn", "S"]
    assert matrix.obj == ["18", "35", "57", "48", "34", "14",
                          "38", "9", "41", "22", "45", "17", "6", "30"]

    user_list = ["48", "57", "18", "35"]  # User input, external labels
    px, pq = acm.generate_setofpairs(matrix.objred, user_list)
    lpx = len(px)
    lpq = len(pq)
    assert lpx == 6
    assert lpq == 6
    # user input necessary , below is a default
    assert px == [(3, 2), (3, 0), (3, 1), (2, 0), (2, 1), (0, 1)]
    assert pq == [(0, 1), (0, 2), (0, 3),
                  (1, 2), (1, 3),
                  (2, 3)]
    res_acm = acm.calc_acm(px, pq)
    assert res_acm == [[0, 0, 0, 0, 1, 0],
                       [0, 0, 1, 0, 0, 0],
                       [0, 1, 1, 1, 1, 0],
                       [1, 0, 0, 0, 0, 0],
                       [0, 1, 0, 0, 0, 0],
                       [0, 0, 0, 1, 0, 0]]

    assert res_acm[0] == [0, 0, 0, 0, 1, 0]
    assert res_acm[1] == [0, 0, 1, 0, 0, 0]
    assert res_acm[2] == [0, 1, 1, 1, 1, 0]
    assert res_acm[3] == [1, 0, 0, 0, 0, 0]
    assert res_acm[4] == [0, 1, 0, 0, 0, 0]
    assert res_acm[5] == [0, 0, 0, 1, 0, 0]

    rowsumacm, colsumacm = acm.calc_obj_attprofile()
    assert rowsumacm[0] == 1
    assert rowsumacm[1] == 1
    assert rowsumacm[2] == 4
    assert colsumacm == [1, 2, 2, 2, 2, 0]

def test_optim_acm(data):
    acm = data[0]
    matrix = data[1]

    user_list = ["48", "57", "18", "35"]
    px, pq = acm.generate_setofpairs(matrix.objred, user_list)
    acm.calc_acm(px, pq)
    rowsumacm, colsumacm = acm.calc_obj_attprofile()
    maxrowsum, maxcolsum = acm.calc_optimum()
    mobjobj, mattatt = acm.find_optimalpairs(px, pq)

    assert maxrowsum == 4
    assert maxcolsum == 2
    assert mobjobj == [(3, 1)]
    assert mattatt == [(0, 2), (0, 3), (1, 2), (1, 3)]

def test_acm_graphics(data):
    acm = data[0]
    matrix = data[1]
    
    user_list = ["48", "57", "18", "35"]

    px, pq = acm.generate_setofpairs(matrix.objred, user_list)

    res_acm = acm.calc_acm(px, pq)

    rowsumacm, colsumacm = acm.calc_obj_attprofile()
    maxrowsum, maxcolsum = acm.calc_optimum()
    mobjobj, mattatt = acm.find_optimalpairs(px, pq)

    assert res_acm == [[0, 0, 0, 0, 1, 0],
                       [0, 0, 1, 0, 0, 0],
                       [0, 1, 1, 1, 1, 0],
                       [1, 0, 0, 0, 0, 0],
                       [0, 1, 0, 0, 0, 0],
                       [0, 0, 0, 1, 0, 0]]

    name_ordinate1, \
        name_ordinate2, \
        labels_obj, \
        labels_att, \
        labelmaxobj, \
        labelmaxatt = acm.acm_graphics(matrix.objred, matrix.prop)

    # Pk, VP: rowsumacm and #colsumacm are the lists
    # with numerical data needed...
    # to draw the two bar diagrams:

    # One bar diagram: ordinate1 and
    # (abcissa1) labels_obj and (values): rowsumacm

    # The other bar diagram: ordinate2 and (abcissa2)
    # labels_att and (values) : colsumacm

    assert name_ordinate1 == "count(incomparabilities) object-pairs"
    assert name_ordinate2 == "count(incomparabilities) att-pairs"
    assert labels_obj == [('48', '57'), ('48', '18'), ('48', '35'),
                          ('57', '18'), ('57', '35'), ('18', '35')]
    assert labels_att == [('Pb', 'Cd'), ('Pb', 'Zn'), ('Pb', 'S'),
                          ('Cd', 'Zn'), ('Cd', 'S'), ('Zn', 'S')]
    assert mobjobj == [(3, 1)]
