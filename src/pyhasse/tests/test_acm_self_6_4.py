# content of test_acm.py
"""Tests for acm.py"""
from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.acm.calc import ACM
import os
import pytest

@pytest.fixture
def data():
    TESTFILENAME = '/data/example_r6_k4_fresh.csv'
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath+TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    acm = ACM(matrix.data, matrix.rows, matrix.cols)
    return acm, matrix

def test_acm(data):
    """Test all operations in acm"""

    acm = data[0]
    matrix = data[1]

    assert matrix.prop == ["q1", "q2", "q3", "q4"]
    assert matrix.obj == ["a", "b", "c", "d", "e", "f"]
    user_list = ["a", "b", "d", "e"]
    px, pq = acm.generate_setofpairs(matrix.objred, user_list)
    lpx = len(px)
    lpq = len(pq)
    assert lpx == 6
    assert lpq == 6
    # user input necessary , below is a default
    assert px == [(0, 1), (0, 3), (0, 4), (1, 3), (1, 4), (3, 4)]
    assert pq == [(0, 1), (0, 2), (0, 3),
                  (1, 2), (1, 3),
                  (2, 3)]
    ac = acm.calc_acm(px, pq)
    # ac is a lpx*lpq-matrix
    assert ac[0] == [1, 1, 0, 0, 1, 1]
    assert ac[1] == [1, 0, 0, 1, 1, 0]
    assert ac[2] == [1, 0, 0, 1, 1, 0]
    assert ac[3] == [1, 0, 1, 1, 0, 1]
    assert ac[4] == [0, 0, 0, 0, 0, 0]
    assert ac[5] == [0, 0, 0, 0, 0, 0]

    rowsumacm, colsumacm = acm.calc_obj_attprofile()
    assert rowsumacm[0] == 4
    assert rowsumacm[1] == 3
    assert rowsumacm[2] == 3
    assert rowsumacm[3] == 4
    assert rowsumacm[4] == 0
    assert rowsumacm[5] == 0
    assert colsumacm == [4, 1, 1, 3, 3, 2]

def test_optim_acm(data):

    acm = data[0]
    matrix = data[1]
                   
    user_list = ["a", "b", "d", "e"]
    px, pq = acm.generate_setofpairs(matrix.objred, user_list)
    acm.calc_acm(px, pq)
    rowsumacm, colsumacm = acm.calc_obj_attprofile()
    maxrowsum, maxcolsum = acm.calc_optimum()
    mobjobj, mattatt = acm.find_optimalpairs(px, pq)
    assert maxrowsum == 4
    # assert mobjobj == ["a", "e"]
    # assert mattatt == [0]

def test_acm_graphics(data):

    acm = data[0]
    matrix = data[1]

    assert matrix.prop == ["q1", "q2", "q3", "q4"]
    user_list = ["a", "b", "d", "e"]

    px, pq = acm.generate_setofpairs(matrix.objred, user_list)
    ac = acm.calc_acm(px, pq)

    # Pk, VP: rowsumacm and #colsumacm are the lists
    # with numerical data needed...
    # to draw the two bar diagrams:
    # One bar diagram: ordinate1 and
    # (abcissa1) labels_obj and (values): rowsumacm
    # The other bar diagram: ordinate2 and (abcissa2)
    # labels_att and (values) : colsumacm

    rowsumacm, colsumacm = acm.calc_obj_attprofile()
    maxrowsum, maxcolsum = acm.calc_optimum()
    mobjobj, mattatt = acm.find_optimalpairs(px, pq)

    assert ac == [[1, 1, 0, 0, 1, 1],
                  [1, 0, 0, 1, 1, 0],
                  [1, 0, 0, 1, 1, 0],
                  [1, 0, 1, 1, 0, 1],
                  [0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0]]
    name_ordinate1, \
        name_ordinate2, \
        labels_obj, \
        labels_att, \
        labelmaxobj, \
        labelmaxatt = acm.acm_graphics(matrix.objred, matrix.prop)

    assert name_ordinate1 == "count(incomparabilities) object-pairs"
    assert name_ordinate2 == "count(incomparabilities) att-pairs"
    assert labels_obj == [('a', 'b'), ('a', 'd'), ('a', 'e'),
                          ('b', 'd'), ('b', 'e'), ('d', 'e')]
    assert labels_att == [('q1', 'q2'), ('q1', 'q3'), ('q1', 'q4'),
                          ('q2', 'q3'), ('q2', 'q4'), ('q3', 'q4')]
