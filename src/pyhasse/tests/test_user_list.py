# content of test_acm.py
"""Tests for acm.py"""
from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.acm.calc import ACM
import os
import pytest


@pytest.fixture
def data():
    TESTFILENAME = '/data/example_r6_k4_fresh.csv'
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath+TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    acm = ACM(matrix.data, matrix.rows, matrix.cols)
    return acm, matrix

def test_acm_user_list_laenge(data):

    acm = data[0]
    matrix = data[1]

    assert matrix.prop == ["q1", "q2", "q3", "q4"]
    user_list = ["a", "b", "c", "d", "e", "f"]
    assert matrix.objred == ['a', 'b', 'c', 'd', 'e', 'f']

    px, pq = acm.generate_setofpairs(matrix.objred, user_list)
    lpx = len(px)
    lpq = len(pq)
    ac = acm.calc_acm(px, pq)
    assert lpx == 15
    assert lpq == 6
    assert ac == [[1, 1, 0, 0, 1, 1],
                  [1, 0, 0, 1, 1, 0],
                  [1, 0, 0, 1, 1, 0],
                  [1, 0, 0, 1, 1, 0],
                  [0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0],
                  [1, 0, 1, 1, 0, 1],
                  [0, 0, 0, 0, 0, 0],
                  [0, 1, 0, 1, 0, 1],
                  [0, 0, 0, 0, 0, 0],
                  [0, 0, 1, 0, 1, 1],
                  [0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0],
                  [1, 0, 0, 1, 1, 0],
                  [0, 0, 0, 0, 0, 0]]
    
