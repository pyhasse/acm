"""Tests for acm module"""

from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.acm.calc import ACM
import os
import pytest

@pytest.fixture
def data():
    TESTFILENAME = '/data/check-K.csv'
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath+TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    acm = ACM(matrix.data, matrix.rows, matrix.cols)
    return acm, matrix

def test_acm(data):
    """Test all operations in acm"""
    acm = data[0]
    matrix = data[1]

    assert matrix.prop == ['q1', 'q2']
    assert matrix.obj == ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
    user_list = matrix.objred
    px, pq = acm.generate_setofpairs(matrix.obj, user_list)
    lpx = len(px)
    lpq = len(pq)
    assert lpx == 10
    assert lpq == 1
    # user input necessary , below is a default
    assert px == [(0, 1), (0, 2), (0, 5), (0, 6), (1, 2),
                  (1, 5), (1, 6), (2, 5), (2, 6), (5, 6)]
    assert pq == [(0, 1)]
    res_acm = acm.calc_acm(px, pq)
    assert res_acm == [
        [0],
        [0],
        [0],
        [0],
        [1],
        [0],
        [0],
        [0],
        [0],
        [1]]

    rowsumacm, colsumacm = acm.calc_obj_attprofile()
    assert rowsumacm[0] == 0
    assert rowsumacm[1] == 0
    assert rowsumacm[2] == 0
    assert colsumacm == [2]

def test_optim_acm(data):

    acm = data[0]
    matrix = data[1]

    user_list = matrix.objred
    px, pq = acm.generate_setofpairs(matrix.objred, user_list)
    acm.calc_acm(px, pq)
    rowsumacm, colsumacm = acm.calc_obj_attprofile()
    maxrowsum, maxcolsum = acm.calc_optimum()
    mobjobj, mattatt = acm.find_optimalpairs(px, pq)

    assert maxrowsum == 1
    assert maxcolsum == 2
    assert mobjobj == [(1, 2), (2, 3)]
    assert mattatt == [(0, 1)]

def test_acm_graphics(data):

    acm = data[0]
    matrix = data[1]

    user_list = matrix.objred

    px, pq = acm.generate_setofpairs(matrix.objred, user_list)

    res_acm = acm.calc_acm(px, pq)

    rowsumacm, colsumacm = acm.calc_obj_attprofile()
    maxrowsum, maxcolsum = acm.calc_optimum()
    mobjobj, mattatt = acm.find_optimalpairs(px, pq)

    assert res_acm == [[0], [0], [0], [0], [1], [0], [0], [1], [0], [0]]

    name_ordinate1, \
        name_ordinate2, \
        labels_obj, \
        labels_att, \
        labelmaxobj, \
        labelmaxatt = acm.acm_graphics(matrix.objred, matrix.prop)

    # Pk, VP: rowsumacm and #colsumacm are the lists
    # with numerical data needed...
    # to draw the two bar diagrams:

    # One bar diagram: ordinate1 and
    # (abcissa1) labels_obj and (values): rowsumacm

    # The other bar diagram: ordinate2 and (abcissa2)
    # labels_att and (values) : colsumacm

    assert name_ordinate1 == "count(incomparabilities) object-pairs"
    assert name_ordinate2 == "count(incomparabilities) att-pairs"
    assert labels_obj == [('a', 'b'), ('a', 'c'), ('a', 'f'),
                          ('a', 'g'), ('b', 'c'), ('b', 'f'),
                          ('b', 'g'), ('c', 'f'), ('c', 'g'), ('f', 'g')]
    assert labels_att == [('q1', 'q2')]
    assert mobjobj == [(1, 2), (2, 3)]
