"""Tests for acm.py"""

from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.acm.calc import ACM
import os
import pytest


@pytest.fixture
def data():
    TESTFILENAME = '/data/zeta_test.csv'
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath+TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    acm = ACM(matrix.data, matrix.rows, matrix.cols)
    return acm, matrix

def test_acm_user_list_zeta(data):
    
    acm = data[0]
    matrix = data[1]

    assert matrix.prop == ["q1", "q2"]

    user_list = ["a", "b", "c", "f"]
    px, pq = acm.generate_setofpairs(matrix.objred, user_list)
    lpx = len(px)
    lpq = len(pq)
    ac = acm.calc_acm(px, pq)

    assert lpx == 6
    assert lpq == 1
    assert ac == [[1], [0], [0], [0], [1], [0]]

    rowsumacm, colsumacm = acm.calc_obj_attprofile()
    maxrowsum, maxcolsum = acm.calc_optimum()
    mobjobj, mattatt = acm.find_optimalpairs(px, pq)

    name_ordinate1, \
        name_ordinate2, \
        labels_obj, \
        labels_att, \
        labelmaxobj, \
        labelmaxatt = acm.acm_graphics(matrix.objred, matrix.prop)

    assert name_ordinate1 == "count(incomparabilities) object-pairs"
    assert name_ordinate2 == "count(incomparabilities) att-pairs"
    assert labels_obj == [('a', 'b'), ('a', 'c'), ('a', 'f'),
                          ('b', 'c'), ('b', 'f'), ('c', 'f')]
    assert labels_att == [('q1', 'q2')]
    assert mobjobj == [(0, 1), (1, 3)]
