# -*- coding: utf-8 -*-

""" Module

pyhasse.acm -- Analysis of conflicts 
(Antichain: a subset of the set of objects, mutually incomparable) .
"""

from pyhasse.acm import calc

name = "pyhasse.acm"

__author__ = """Rainer Bruggemann"""
__email__ = 'rainer.bruggemann@pyhasse.org'
__version__ = '0.2.7'
